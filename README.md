# Snowdrift.coop governance overview

Each role has explicit Accountabilities and Policies at their linked
descriptions. For a general summary of this governance concept, see our
[governance wiki page].

[governance wiki page]: https://wiki.snowdrift.coop/governance

## Decision-making processes

We use various tools and processes in our governance. An incomplete list:

- [Process for consent decisions on the forum](decision-making/forum-consent-polls.md)

## Circles and roles

The listings below describe our governance structure.

### [Main Circle] — *Enable sustainable funding of public goods.*

- Lead (a.k.a, in co-op terms: "General Manager", a.k.a. CEO) \([wolftune])
- Elected governance roles[^1]
    - Facilitator \()
    - Secretary \()
    - Rep to Board of Directors \()[^2]
- [Branding] \([mray]) — *Making sure that Snowdrift.coop has a consistent and recognizable visual style.*
- [Strategic Vision] \([wolftune]) — *Keeping publications and plans in line with our core values and mission and provide guidance on communicating those values to the public.*
    -  **Domain:** Official/published materials

[^1]: Elections are done by all the other partners holding roles in a Circle,
    including the Leads from any immediate sub-circles.

[^2]: Note about leads and reps: The lead and rep both report to higher circles
    (Board in this case), but the lead focuses on the directives that come from
    the higher circle (bringing those to the circle they lead), while the rep
    focuses on communicating tensions from their circle to the higher circle.

#### Top-level roles

- [Human Resources] \([])
- [Legal] \([])
- [Security] \([Salt], [chreekat])
    - **Domains:** ??
- [Sysadmin] \([Salt], [chreekat]) — *Choosing, deploying, and maintaining technical infrastructure.*
    - **Domain:** Maintainance, updates, deployment of tech infrastructure
- [Treasurer] \([wolftune]) — *Making sure money is accounted for and spent wisely (internally).*
    - **Domains:** Expenditures, accounting, budget reports

#### [Outreach Circle]

Purpose: *Promote the Snowdrift.coop brand; recruit projects, partners, and
    volunteers; engage with the broader community.*

- Outreach Lead \(wolftune)
- Elected governance roles
    - Facilitator \()
    - Secretary \()
    - Rep to Main Circle \()
    - Rep to Website Circle \()
- [Copywriting] \([wolftune]) — *Writing as requested by other roles.*
- [Design] \([mray], [ikomi], [msiep]) — *Creating designs and assets.*
- [DevOps] \() — *Customizing applications to suit Snowdrift.coop's unique needs.*
- [Liaison] \([Salt], [wolftune]) — *Handled connections with individuals or projects looking to create a stronger bond with Snowdrift.coop.*
- [Market Research] \([wolftune]) — *Trust-building (showing the public that we've done our homework).*
- [Moderation] \([Salt])— *Foster community engagement, facilitate healthy interactions.*
- [Publicity] \([Salt], [wolftune]) — *Networking to increase credibility, awareness, brand recognition, and website traffic.*

#### [Website Circle]

Purpose: *Build the functioning Snowdrift.coop web platform.*

- Website Lead \([chreekat])
- Elected governance roles
    - Facilitator \()
    - Secretary \()
    - Rep to Main Circle \()
    - Rep from Outreach Circle \([wolftune])
- [Architect] \([chreekat]) — *A core architecture for Snowdrift that enables rapid development and integrity of the codebase.*
    - **Domain:** Snowdrift technology stack
- [Code QA] \([chreekat], [singpolyma]?) — *Making sure we have high quality code.*
    - **Domain:** Coding style
- [Licensing] \( ) — maintaining FLO licensing standards
- [Web Developer] \([fr33domlover], [h30]) — *Delivering features etc.*
- [Interaction Design] \(**[msiep]**, [mray]) — *Designing effective, quality User Experience (UX)*
    - **Domain:** Page list, User Stories, design criteria
- [Visual Design] \([mray]) — *Website design is coherent and consistent with our brand.*
    - **Domain:** Website design framework, visual style


[Main Circle]: Main-Circle.md
[General Manager]: Main-Circle.md#lead
[Human Resources]: Main-Circle.md#human-resources
[Legal]: Main-Circle.md#legal
[Security]: Main-Circle.md#security
[Sysadmin]: Main-Circle.md#sysadmin
[Treasurer]: Main-Circle.md#treasurer

[Outreach Circle]: Outreach-Circle.md
[Outreach Lead]: Outreach-Circle.md#lead-link
[Branding]: Outreach-Circle.md#branding
[Copywriting]: Outreach-Circle.md#copywriter
[Design]: Outreach-Circle.md#designer
[DevOps]: Outreach-Circle.md#developer
[Liaison]: Outreach-Circle.md#liaison
[Market Research]: Outreach-Circle.md#market-research
[Moderation]: Outreach-Circle.md#moderation
[Publicity]: Outreach-Circle.md#publicity
[Strategic Vision]: Outreach-Circle.md#strategic-vision

[Website Circle]: Website-Circle.md
[Website Lead]: Website-Circle.md#lead-link
[Architect]: Website-Circle.md#architect
[Code QA]: Website-Circle.md#code-qa
[Licensing]: Website-Circle.md#licensing
[Web Developer]: Website-Circle.md#developer
[Interaction Design]: Website-Circle.md#interaction-design
[Visual Design]: Website-Circle.md#visual-design

[wolftune]: https://wiki.snowdrift.coop/community/team
[ikomi]: https://wiki.snowdrift.coop/community/team
[Salt]: https://wiki.snowdrift.coop/community/team
[mray]: https://wiki.snowdrift.coop/community/team
[chreekat]: https://wiki.snowdrift.coop/community/team
[JazzyEagle]: https://wiki.snowdrift.coop/community/team
[smichel17]: https://wiki.snowdrift.coop/community/team
[msiep]: https://wiki.snowdrift.coop/community/team
[singpolyma]: https://wiki.snowdrift.coop/community/team
[h30]: https://community.snowdrift.coop/u/h30
[fr33domlover]: https://community.snowdrift.coop/u/fr33domlover
