# (Potential) Roles / circles (updated and/or new)

*sort through this set of drafts toward the final structure*

## General Manager

## Board of Directors
primary initial goal: seeing through the drafting and acceptance of initial Bylaws in conjunction with legal team; after that, Board will be elected per the Bylaws.

Traits desired:
- FLO knowledge/sensitivity
- co-op, democracy, equity political values, insights
- experience in advising, serving on Boards
- software/web development
- sensitivity, experience regarding distributed global team and volunteers
- fundraising experience
- diversity among Board members
- accountabilities:
    - monthly meetings
    - reading relevant email and forum updates
    - availability for some between-meeting advising

## Treasurer
including accounting, tax filing, and finance in general

## Secretary


## Co-op developer
Lead our specific co-op implementation and the running of the co-op democratically etc., engagement with general membership, newsletter?

## Human support / assistant
Helping support the other team members, listen to concerns, provide coaching and support, facilitate good communication, regular 1-on-1 check-ins, whatever can be done to encourage others and help everyone succeed. Know who's who in the team etc. Onboarding of newcomers.
*Note: this is sort of related to traditional "human resources" role but we don't want that term

Note: this role should help others pair up to have pair accountability etc.

## Facilitator / mods
Neutral support for the community in the forum and elsewhere, someone actively willing to flag, to contact people with grace about concerns, help run meetings

## Project manager
prioritizing tasks, backlog grooming, tracking milestones, coordinating related tasks

## Development (building our product)
- CTO / technical lead — architecture, tech stack, managing code contributors etc.
- Programmers
    - Frontend protyping, implementation (html/sass/js)
    - Backend (Haskell — and/or potentially others if discussed and agreed)
- Testers / Reviewers

## Organizational management / Operations
Maintaining working systems and business etc.
- sysadmin (including compliance, security etc)

## Outreach
covers acquisition/marketing and support/recruiting/community…
- Blog editor / publicist (solicit posts from others, guide and edit etc)
- CiviCRM manager
- press liaison, managing questions, customer service etc

## Lawyers and legal help:
- web platform knowledge (terms, liabilities, etc)
- co-op issues
- financial, tax, donation issues, esp. international
