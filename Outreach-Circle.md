# Snowdrift.coop Outreach Circle

Outreach overall covers most Snowdrift.coop activity outside the direct
development and operating of the main Snowdrift.coop site.

## Roles

### Branding

- Maintaining visual style guidelines
- Reviewing media we publish for alignment with our visual style

#### Branding Policies

**Brand Guidelines**

* See the [Design Guide](https://gitlab.com/snowdrift/design/blob/master/docs/design-guide/readme.md)

### Copywriter

- Writing and/or reviewing text on publicity materials and elsewhere
- Managing text assets outside the main site, e.g. the [wiki](https://wiki.snowdrift.coop)

### Designer

- Creating promotional material (flyers, stickers, t-shirts, etc)
- design / swag / whatever
- Managing visual assets
- Deciding on the look and feel of infrastructure customizations

### Developer

- Development work to customize any platforms/tools used by Outreach

### Fundraising

- Identifying and applying for grants
- Coordinating pre-launch fund-drives / other fundraising
- Tracking donations and follow-ups in CiviCRM

### Liaison

- Recruiting partner projects
- Growing & directing volunteer community
- Managing CiviCRM, including entering contact info
- Networking with partner organizations
- Maintain https://wiki.snowdrift.coop/community/how-to-help

### Market Research

- conducting, analyzing, and presenting research about the broader ecosystem
  outside of Snowdrift.coop, the history of public goods and related concerns

### Moderation

- Decide on and enforce rules
- IRC Channel welcomer
- Publishing co-op member newsletter
- Obtaining co-op member feedback

### Publicity

- Representing Snowdrift.coop at events e.g. speaking at conferences, expos, other organizations
- Giving talks
- Interviews/podcasts
- Publishing blog
- Press releases

### Strategic Vision

- Defining communications strategy
- Reviewing media we publish for alignment with our vision
