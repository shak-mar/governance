# Roles in All Cirles

These boilerplate definitions apply to all Circles, although the roles may be
filled by different team members in each case.

## Lead

The Lead creates the rest of the Circle roles and is ultimately accountable for
the whole Circle. The Lead is also a member of the parent Circle accountable for
taking direction from the parent and communicating it to the Lead's own Circle.

Accountabilities:

- Structuring the Circle
- Assigning Partners to Roles
    - monitoring the fit
    - offering feedback and guidance for roles
    - re-assigning Roles as needed
- Allocating resources across various Projects and/or Roles
- Establishing overall priorities and Strategies
- Holding all un-delegated Circle-level Domains and Accountabilities

## Representative(s)

A Rep (for short) role is one that represents a Circle in meetings and
discussion with another circle. All Circles have a Rep that communicates the
Circle's concerns to the higher Circle (and for the Main Circle, to the Board of
Directors). Some Circles also have Reps to other Circles.

## Facilitator

Accountabilities:

- Run meetings
- Moderate non-real-time communications
- Educating others about and adhering to governance principles

## Secretary

- Schedule meetings
- Notify team members about meetings and other announcements
- Note-taking during meetings
- Updating records
